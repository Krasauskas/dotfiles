#!/usr/bin/env zsh

set -euxo pipefail

# Why not bindmount? Multi-device use becomes messy. Now I can have a new config
# file without it being part of git repo.

local GITCONF=config
local CONFIG=~/.config

# ZSH
local GITZSHCONF=$GITCONF/zsh
local ZSHCONF=$CONFIG/zsh
mkdir -p $ZSHCONF
ln -f zshrc ~/.zshrc
ln -f $GITZSHCONF/custom.sh $ZSHCONF
git clone https://github.com/ohmyzsh/ohmyzsh $ZSHCONF/oh-my-zsh
wget --background https://raw.githubusercontent.com/caiogondim/bullet-train.zsh/master/bullet-train.zsh-theme --output-document $ZSHCONF/ohmyzsh/custom/themes/bullet-train.zsh-theme

# Alacritty
local GITALACCONF=$GITCONF/alacritty
local ALACCONF=$CONFIG/alacritty
mkdir -p $ALACCONF
ln -f -t $ALACCONF $GITALACCONF/*.toml

# Tmux
local GITTMUXCONF=$GITCONF/tmux
local TMUXCONF=$CONFIG/tmux
mkdir -p $TMUXCONF/plugins
ln -f $GITTMUXCONF/tmux.conf $TMUXCONF
[ ! -d $TMUXCONF/plugins/tpm ] && \
	git clone --sparse https://github.com/tmux-plugins/tpm $TMUXCONF/plugins/tpm

# Emacs
local GITEMACSCONF=$GITCONF/emacs
local EMACSCONF=$CONFIG/emacs
mkdir -p $EMACSCONF/{backup,config/modes,snippets}
ln -f -t $EMACSCONF $GITEMACSCONF/*.el
ln -f -t $EMACSCONF/config $GITEMACSCONF/config/*.el
ln -f -t $EMACSCONF/config/modes $GITEMACSCONF/config/modes/*.el
for mode in $(ls $GITEMACSCONF/snippets)
do
    mkdir -p $EMACSCONF/snippets/$mode
    ln -f -t $EMACSCONF/snippets/$mode $GITEMACSCONF/snippets/$mode/*
done

# Sway
local GITSWAYCONF=$GITCONF/sway
local SWAYCONF=$CONFIG/sway
mkdir -p $SWAYCONF/config.d
ln -f $GITSWAYCONF/config $SWAYCONF
ln -f -t $SWAYCONF/config.d $GITSWAYCONF/config.d/*.conf

## Swaylock
local GITSWAYLOCK=$GITCONF/swaylock
local SWAYLOCK=$CONFIG/swaylock
mkdir -p $SWAYLOCK
ln -f -t $SWAYLOCK $GITSWAYLOCK/*

## Swaylock
local GITSWAYIDLE=$GITCONF/swayidle
local SWAYIDLE=$CONFIG/swayidle
mkdir -p $SWAYIDLE
ln -f -t $SWAYIDLE $GITSWAYIDLE/*

## Mako
mkdir -p $CONFIG/mako
ln -f $GITCONF/mako/config $CONFIG/mako/

## Waybar
mkdir -p $CONFIG/waybar
ln -f $GITCONF/waybar/config $CONFIG/waybar

## MPV
mkdir -p $CONFIG/mpv
ln -f $GITCONF/mpv/mpv.conf $CONFIG/mpv

## SSH
local SSHCONF=~/.ssh
mkdir -p ~/.ssh/config.d
ln -f $GITCONF/ssh/config $SSHCONF/
ln -f -t $SSHCONF/config.d $GITCONF/ssh/config.d/*

## keyboard layout
local XKBCONF="~/.xkb/symbols"
mkdir -p $XKBCONF
ln -f -t $XKBCONF $GITCONF/xkb/symbols/*
