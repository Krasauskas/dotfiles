#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

# function cleanup() {
#     echo "Cleaning things up"
# }

# trap cleanup SIGHUP SIGINT SIGQUIT SIGABRT EXIT

timestamp=$(date "+%F.%H:%M:%S")
# screen id from xwininfo. I think this is the combo screen, not the individual
screen=0x2c0002e
convert x:"$screen" x: Pictures/ss."$timestamp".png
