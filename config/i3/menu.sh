#!/bin/zsh

exec i3-msg -q "exec --no-startup-id $(compgen -c | sort -u | fzf)"
