export EDITOR="emacsclient --tty"
export COLORTERM=truecolor
export XDG_CONFIG_HOME=$HOME/.config
export PATH="$HOME/.local/bin/:${PATH}"
export GDK_BACKEND=wayland

# oh-my-zsh
# plugins=(
#     aws
#     chucknorris
#     colored-man-pages
#     git
#     kubectl
#     kubectx
#     ripgrep
#     terraform
# )

# oh-my-zsh/kubectx
#RPS1='$(kubectx_prompt_info)'
# kubectx_mapping["minikube"]="mini"
# kubectx_mapping["context_name_from_kubeconfig"]="$emoji[wolf_face]"
# kubectx_mapping["production_cluster"]="%{$fg[yellow]%}prod!%{$reset_color%}"

# oh-my-zsh/terraform
RPROMPT='$(tf_prompt_info)'
#RPROMPT='$(tf_version_prompt_info)'
ZSH_THEME_TF_PROMPT_PREFIX="%{$fg[white]%}"
ZSH_THEME_TF_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_TF_VERSION_PROMPT_PREFIX="%{$fg[white]%}"
ZSH_THEME_TF_VERSION_PROMPT_SUFFIX="%{$reset_color%}"

umask 077

alias ec="emacsclient --tty"
alias copy="rsync --archive --progress"
alias launch-journal="emacsclient --tty -e '(example-split-window-2-files \"journal.org\" \"todo.org\")'"
alias kate="kubectl"
alias tf-latest="tfenv list-remote | head -5"
alias x=kubectx
alias kate=kubectl

function send-email() {
	echo "$4" | mail -r $1 -s $3 $2
}

function getWeather() {
    declare -r weatherRun="/tmp/weatherRun"
    declare lastRun timeout current
    [ -f $weatherRun ] || echo 0 > $weatherRun
    lastRun=$(cat $weatherRun)
    current=$EPOCHSECONDS
    timeout=3600
    (( lastRun + timeout >= current )) && return 0
    curl https://wttr.in
    echo $current > $weatherRun
}

function ssh-agent-setup() {
    # SSH agent
    pid_file="$HOME/.ssh/ssh-agent.pid"
    SSH_AUTH_SOCK="$HOME/.ssh/ssh-agent.sock"
    if [ -z "$SSH_AGENT_PID" ]
    then
      # no PID exported, try to get it from pidfile
      SSH_AGENT_PID=$(cat "$pid_file")
    fi

    if ! kill -0 "$SSH_AGENT_PID" &> /dev/null
    then
      # the agent is not running, start it
      rm "$SSH_AUTH_SOCK" &> /dev/null
      >&2 echo "Starting SSH agent, since it's not running; this can take a moment"
      eval "$(ssh-agent -s -a "$SSH_AUTH_SOCK")"
      echo "$SSH_AGENT_PID" > "$pid_file"

      >&2 echo "Started ssh-agent with '$SSH_AUTH_SOCK'"
    fi
    export SSH_AGENT_PID
    export SSH_AUTH_SOCK
}

function createTmuxSession() {
    set -x
    declare sshConfig="~/.ssh/config.d/20-nq.conf"
    declare prefix
    declare -a hosts newWindowList

    [ "$1" = "LD9" ] && prefix="l-"
    [ "$1" = "VOLTA" ] && prefix="v-"
    [ "$1" = "STAGING" ] && prefix="s-"

    for i in $(rg Host ~/.ssh/config.d/20-nq.conf | rg "$prefix" | cut -d' ' -f2)
    do
	hosts+="$i"
    done

    for host in "${hosts[@]:1}"
    do
	declare windowName="${host:2}"
	newWindowList+=( new-window ssh "$host" ';' rename-window "$windowName" ';')
    done

    tmux new-session -s "$1" ssh "${hosts[1]}" ';' rename-window "${hosts[1]:2}" ';'\
	 "${newWindowList[@]}" \
	 select-layout tiled ';' \
}

function nqtmux() {
    [ -n "${1}" ] || return 1
    declare sessionName
    [ "$SHELL" = "/bin/bash" ] && sessionName="${1^^}"
    [ "$SHELL" = "/bin/zsh" ] && sessionName="${1:u}"
    tmux has-session -t "$sessionName" && \
	tmux attach -t "$sessionName" || \
	    createTmuxSession "$sessionName"
    set +x
}

function reset-nic(){
    declare -r nic="${1:-enp0s31f6}"
    echo "Resetting $nic"
    sudo ip link set "$nic" down
    sudo ip link set "$nic" up
    echo "Done"
}

ssh-agent-setup

if [ "$(tty)" = "/dev/tty1" ]; then
    if test -z "${XDG_RUNTIME_DIR}"; then
	export XDG_RUNTIME_DIR=/tmp/${UID}-runtime-dir
	if ! test -d "${XDG_RUNTIME_DIR}"; then
            mkdir "${XDG_RUNTIME_DIR}"
            chmod 0700 "${XDG_RUNTIME_DIR}"
	fi
    fi
     exec dbus-run-session Hyprland
#     export XKB_DEFAULT_OPTIONS=caps:escape
#     sway
fi

if [ "$(tty)" = "/dev/tty2" ]; then
    if test -z "${XDG_RUNTIME_DIR}"; then
	export XDG_RUNTIME_DIR=/tmp/${UID}-runtime-dir
	if ! test -d "${XDG_RUNTIME_DIR}"; then
            mkdir "${XDG_RUNTIME_DIR}"
            chmod 0700 "${XDG_RUNTIME_DIR}"
	fi
    fi
#     exec dbus-run-session Hyprland
    #     export XKB_DEFAULT_OPTIONS=caps:escape
    export XDG_CURRENT_DESKTOP=sway
    dbus-run-session sway
fi

for file in ~/.zshrc ~/.config/zsh/*.sh
do
    [[ $file -ot $file.zwc ]] || zcompile $file
done

autoload -U compinit promptinit
compinit
zstyle ':completion:complete:*' use-cache 1

fastfetch
#getWeather
