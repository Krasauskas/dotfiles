;;; Load all ".el" files under ~/.emacs.d/config directory.
;; Following instructions on https://www.emacswiki.org/emacs/DotEmacsModular
(load "~/.config/emacs/load-directory")
(load-directory "~/.config/emacs/config")

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages '(gitlab-ci-mode use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
