(use-package terraform-mode
  :ensure t
  :custom (terraform-indent-level 2)
  :config
  (defun my-terraform-mode-init ()
    ;; if you want to use outline-minor-mode
    ;; (outline-minor-mode 1)
    (terraform-format-on-save-mode)
    )

  (add-hook 'terraform-mode-hook 'my-terraform-mode-init))

;; (use-package company-terraform)
;; (require 'company-terraform)
;; (company-terraform-init)
