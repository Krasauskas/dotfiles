(use-package lsp-mode
  :ensure t
  :hook ((terraform-mode . lsp-deferred)
	 (sh-mode . lsp)
;	 ('go-mode-hook . lsp-deferred)
	 ))
(require 'lsp-mode)
(add-hook 'go-mode-hook #'lsp-deferred)

;; Set up before-save hooks to format buffer and add/delete imports.
;; Make sure you don't have other gofmt/goimports hooks enabled.
(defun lsp-go-install-save-hooks ()
  (add-hook 'before-save-hook #'lsp-format-buffer t t)
  (add-hook 'before-save-hook #'lsp-organize-imports t t))
(add-hook 'go-mode-hook #'lsp-go-install-save-hooks)

(setq lsp-disabled-clients '(tfls))
(setq lsp-terraform-ls-enable-show-reference t)
(setq lsp-semantic-tokens-enable t)
(setq lsp-semantic-tokens-honor-refresh-requests t)
(setq lsp-enable-links t)
(setq lsp-terraform-ls-prefill-required-fields t)

(use-package yasnippet)

(use-package lsp-ui)
(define-key lsp-ui-mode-map [remap xref-find-definitions] #'lsp-ui-peek-find-definitions)
(define-key lsp-ui-mode-map [remap xref-find-references] #'lsp-ui-peek-find-references)

(lsp-ui-mode t)

(use-package ansible)
(add-hook 'yaml-mode-hook 'ansible)
(add-hook 'yaml-mode-hook 'lsp-deferred)


;; C++ LSP
(use-package ccls)
(setq ccls-executable "/usr/bin/ccls")
