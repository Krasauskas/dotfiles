(use-package company)
(add-hook 'after-init-hook 'global-company-mode)
(add-to-list 'company-backends 'company-shell)
(use-package company-flx)
(with-eval-after-load 'company
  (company-flx-mode +1))

(use-package company-go)
(use-package company-terraform
  :hook (terraform-mode . company-terraform-init)
  )

(use-package company-ansible)
(use-package company-shell)
(add-to-list 'company-backends '(company-shell company-shell-env company-ansible))
