;;; interface.el --- Emacs interface configuration
;;; Commentary:
;;

;;; Code:

(add-hook 'prog-mode-hook 'display-line-numbers-mode)
(add-hook 'conf-mode-hook 'display-line-numbers-mode)
;(setq linum-format "%4d\u2502")
(setq linum-format 'linum-highlight-current-line-number)

(add-hook 'text-mode-hook #'auto-fill-mode)
(add-hook 'prog-mode-hook #'auto-fill-mode)
(setq-default fill-column 80)

(column-number-mode)
(menu-bar-mode -1)
(global-hl-line-mode)

(require 'whitespace)
(setq whitespace-style '(face empty tabs lines-tail trailing))
(global-whitespace-mode t)
(global-display-fill-column-indicator-mode)

; Set split window for journal and todo in tmux. Borrowed from https://stackoverflow.com/a/26593564
(defun example-split-window-2-files (f1 f2)
  (find-file f1)
  (find-file-other-window f2))

; Move between windows using arrows
(global-set-key (kbd "C-c <left>")  'windmove-left)
(global-set-key (kbd "C-c <right>") 'windmove-right)
(global-set-key (kbd "C-c <up>")    'windmove-up)
(global-set-key (kbd "C-c <down>")  'windmove-down)

;; line number mode is enabled for "programming" and "conf" modes.
;; See top of file
;(global-display-line-numbers-mode)
;;; interface.el ends here
