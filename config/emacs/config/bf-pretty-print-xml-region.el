;;; bf-pretty-print-xml-region --- Summary"                 │
;;; Commentary:
;;; http://blog.bookworm.at/2007/03/pretty-print-xml-with-emacs.html"
;;; Code:
(defun bf-pretty-print-xml-region (begin end)
  "BEGIN.
Pretty format XML markup in region.  You need to have 'nxml-mode'
http://www.emacswiki.org/cgi-bin/wiki/NxmlMode installed to do
this.  The function inserts linebreaks to separate tags that have
nothing but whitespace between them.  It then indents the markup
by using nxml's indentation rules.
END"
  (interactive "r")
  (save-excursion
      (nxml-mode)
      (goto-char begin)
      (while (search-forward-regexp "\>[ \\t]*\<" nil t)
        (backward-char) (insert "\n") (setq end (1+ end)))
      (indent-region begin end))
    (message "Ah, much better!"))
(provide 'bf-pretty-print-xml-region)
;;; bf-pretty-print-xml-region.el ends here
