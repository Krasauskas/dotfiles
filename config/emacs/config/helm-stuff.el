(use-package async
  :ensure t
  :config
  (async-bytecomp-package-mode 1))

(use-package helm
  :demand t
  :bind (("M-x" . helm-M-x)
	 ("C-x C-f" . helm-find-files)
	 :map helm-find-files-map
         ("<tab>"         . helm-execute-persistent-action)
         ("C-<backspace>" . helm-find-files-up-one-level)
	 )
  :config
  (helm-mode 1)
  (setq completion-styles '(flex))
  )

(use-package helm-flycheck)

(use-package helm-lsp)
(define-key lsp-mode-map [remap xref-find-apropos] #'helm-lsp-workspace-symbol)
