* Usage

Clone the repo somewhere (~\~/git/dotfiles~ comes to mind)

#+begin_src bash
  chmod u+x setup.sh
  ./setup.sh
#+end_src

Use dmesg to figure out if any firmware or microcode is not getting loaded; and if so - find the name.
Following https://wiki.gentoo.org/wiki/Intel_microcode

#+begin_src bash
  dmesg | rg -i 'firmware|microcode'
#+end_src
